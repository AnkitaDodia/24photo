package com.advait.photobook.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.advait.photobook.Adapter.CardAdapter;
import com.advait.photobook.Models.CartData;
import com.advait.photobook.Models.LoginData;
import com.advait.photobook.R;
import com.advait.photobook.Utils.Api;
import com.advait.photobook.Utils.AppController;
import com.advait.photobook.Utils.InternetStatus;
import com.advait.photobook.Utils.SettingValue;
import com.advait.photobook.common.BaseActivity;
import com.advait.photobook.restinterface.RestInterface;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    EditText txtUsername, txtPassword;
    TextView btnLogin,btnReset;

    Context con;
    SettingValue settingValue;
    ProgressDialog dialog;
    boolean isInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        con = this;
        settingValue = SettingValue.getInstance(con);
        settingValue.loadSettingValue();

        isInternet = new InternetStatus().isInternetOn(con);

        dialog = new ProgressDialog(con);
        dialog.setMessage("Vent venligst..");
        dialog.setCancelable(false);

        txtUsername = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnReset = findViewById(R.id.btnReset);

        btnLogin.setOnClickListener(this);
        btnReset.setOnClickListener(this);

//        txtUsername.setText("michael@frosterne.dk");//michael@frosterne.dk //abc@gmail.com
//        txtPassword.setText("24fora80");
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(LoginActivity.this);
        myAlertDialog.setMessage("Er du sikker på at du vil lukke appen?");
        myAlertDialog.setTitle(getResources().getString(R.string.app_name));
        myAlertDialog.setIcon(R.drawable.logo);
        myAlertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface arg0, int arg1) {
                finish();
                System.exit(0);
            }
        });

        myAlertDialog.setNegativeButton("Fortryd", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });

        myAlertDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (txtUsername.getText().length() < 1)
                    txtUsername.setError("Indtast brugnavn");
                else if (txtPassword.getText().length() < 1)
                    txtPassword.setError("Indtast adgangskode");
                else
                if (isInternet) {
//                    LoginApi();
                    sendNewLoginRequest();
                }
                else {
                    Toast.makeText(con, "Ingen internetforbindelse", Toast.LENGTH_LONG).show();
                }


                break;
            case R.id.btnReset:
                startActivity(new Intent(con, ResetPasswordActivity.class));
                finish();
                break;
        }
    }

    private void sendNewLoginRequest()
    {
        dialog = new ProgressDialog(con);
        dialog.setMessage("Vent venligst..");
        dialog.setCancelable(false);
        dialog.show();

        try {
            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);


            String jsonStr = "{\"email\":\""+txtUsername.getText().toString()+"\",\"pw\":\""+txtPassword.getText().toString()+"\",\"device_id\":\""+""+"\",\"fcm\":\""+getFCMID()+"\"}";;

            Log.e("jsonStr LoginData", "==> " + jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendLoginRequest(in, new Callback<LoginData>() {
                @Override
                public void success(LoginData model, retrofit.client.Response response) {
                    if (dialog != null)
                        dialog.dismiss();

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("LOGIN_RESPONSE", "" + new Gson().toJson(model));

                            switch (model.getText()) {
                                case "WrongPassword":
                                    Toast.makeText(con, "Adgangskoden er forkert, nulstil din adgangskode", Toast.LENGTH_SHORT).show();
                                    break;
                                case "WrongEmail":
                                    Toast.makeText(LoginActivity.this, "Emailen er ikke registreret i systemet, tjek venligst og prøv igen", Toast.LENGTH_SHORT).show();
                                    break;
                                case "NoValidMembership":
                                    Toast.makeText(LoginActivity.this, "Du har desværre ikke et gyldigt medlemskab", Toast.LENGTH_SHORT).show();
                                    break;
                                case "ValidMembership":
                                    settingValue.saveSetting(settingValue.str_isLogin, true);
                                    settingValue.saveSetting(settingValue.str_name,model.getName());
                                    settingValue.saveSetting(settingValue.str_UserId,model.getUserId());
                                    settingValue.saveSetting(settingValue.str_AvailablePhotos,model.getAvailablePhotos());
                                    settingValue.saveSetting(settingValue.str_FreeShipments,model.getFreeShipments());
                                    settingValue.saveSetting(settingValue.str_PriceExtraPhoto,model.getPriceExtraPhoto());
                                    settingValue.saveSetting(settingValue.str_PriceExtraShipment,model.getPriceExtraShipment());
                                    settingValue.saveSetting(settingValue.str_UrlPhotoRaw, model.getUrlPhotoRaw());
                                    settingValue.saveSetting(settingValue.str_UrlPhotoDevelop,model.getUrlPhotoDevelop());
                                    settingValue.saveSetting(settingValue.str_UrlPhotoThumb,model.getUrlPhotoThumb());
                                    settingValue.saveSetting(settingValue.str_DateNextMembershipRenewal, model.getDateNextMembershipRenewal());

                                    settingValue.saveSetting(settingValue.str_MaxPhotoSize, model.getMaxPhotoSize());

                                    settingValue.saveSetting(settingValue.str_Email,txtUsername.getText().toString());
                                    settingValue.saveSetting(settingValue.str_Password,txtPassword.getText().toString());

                                    startActivity(new Intent(con, NewBottomActivity.class));
                                    finish();
                                    break;
                            }
                        } catch (Exception e) {

                            if (dialog != null)
                                dialog.dismiss();
                            e.printStackTrace();

                            Toast.makeText(con,"Noget gik galt. Prøv igen senere",Toast.LENGTH_LONG).show();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error)
                {
                    if (dialog != null)
                        dialog.dismiss();
                    Log.e("ERROR", "" + error.getMessage());
                    Toast.makeText(con,"Noget gik galt. Prøv igen senere",Toast.LENGTH_LONG).show();
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            if (dialog != null)
                dialog.dismiss();


            Toast.makeText(con,"Noget gik galt. Prøv igen senere",Toast.LENGTH_LONG).show();
        }
    }

    private void LoginApi() {


            String tag_json_obj = "json_obj_req";

            dialog.show();

            // https://24fotos.dk/API/webservices/login.php?email={{EMAIL}}&pw={{password}}&device_id={{device id}}&fcm={{FCM}}
            String url = Api.baseurl + Api.login + txtUsername.getText().toString() + "&pw=" + txtPassword.getText().toString()
                    +"&device_id="+" "+"&fcm="+getFCMID();//getIMEINumber()

            Log.e("URL", "==> " + url);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,

                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("LOGIN", "==> " + response);

                            try {
                                JSONObject data = new JSONObject(response);
                                switch (data.getString("text")) {
                                    case "WrongPassword":
                                        Toast.makeText(con, "Adgangskoden er forkert, nulstil din adgangskode", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "WrongEmail":
                                        Toast.makeText(LoginActivity.this, "Emailen er ikke registreret i systemet, tjek venligst og prøv igen", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "NoValidMembership":
                                        Toast.makeText(LoginActivity.this, "Du har desværre ikke et gyldigt medlemskab", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "ValidMembership":
                                        settingValue.saveSetting(settingValue.str_isLogin, true);
                                        settingValue.saveSetting(settingValue.str_name,data.getString("name"));
                                        settingValue.saveSetting(settingValue.str_UserId,data.getInt("UserId"));
                                        settingValue.saveSetting(settingValue.str_AvailablePhotos,data.getInt("AvailablePhotos"));
                                        settingValue.saveSetting(settingValue.str_FreeShipments,data.getInt("FreeShipments"));
                                        settingValue.saveSetting(settingValue.str_PriceExtraPhoto,data.getString("PriceExtraPhoto"));
                                        settingValue.saveSetting(settingValue.str_PriceExtraShipment,data.getString("PriceExtraShipment"));
                                        settingValue.saveSetting(settingValue.str_UrlPhotoRaw,data.getString("UrlPhotoRaw"));
                                        settingValue.saveSetting(settingValue.str_UrlPhotoDevelop,data.getString("UrlPhotoDevelop"));
                                        settingValue.saveSetting(settingValue.str_UrlPhotoThumb,data.getString("UrlPhotoThumb"));
                                        settingValue.saveSetting(settingValue.str_DateNextMembershipRenewal,data.getString("DateNextMembershipRenewal"));
                                        settingValue.saveSetting(settingValue.str_Email,txtUsername.getText().toString());
                                        settingValue.saveSetting(settingValue.str_Password,txtPassword.getText().toString());
                                        startActivity(new Intent(con, NewBottomActivity.class));
                                        finish();
                                        break;
                                }
                            } catch (JSONException e) {
                                if (dialog != null)
                                    dialog.dismiss();
                                Toast.makeText(con,"Noget gik galt. Prøv igen senere",Toast.LENGTH_LONG).show();
                                Log.e("JSONException","JSONException : "+e);
                            } catch (Exception e) {
//                                e.printStackTrace();
                                Log.e("JSONException","JSONException : "+e);
                                Toast.makeText(con,"Noget gik galt. Prøv igen senere",Toast.LENGTH_LONG).show();
                            }
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (dialog != null)
                                dialog.dismiss();
                            Toast.makeText(con,"Noget gik galt. Prøv igen senere",Toast.LENGTH_LONG).show();
                        }
                    }) {
            };

            AppController.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
    }
}