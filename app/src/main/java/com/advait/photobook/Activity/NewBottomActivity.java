package com.advait.photobook.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.advait.photobook.Fragment.FragmentCart;
import com.advait.photobook.Fragment.FragmentHome;
import com.advait.photobook.Fragment.FragmentSetting;
import com.advait.photobook.Fragment.NewHistoryFragment;
import com.advait.photobook.R;
import com.advait.photobook.Utils.SettingValue;
import com.advait.photobook.common.BaseActivity;


public class NewBottomActivity extends BaseActivity implements View.OnClickListener {

    ImageView btn_menu1,btn_menu2,btn_menu3,btn_menu4,btn_menu5;
    RelativeLayout container,btnMenu;

    Context con;
    SettingValue settingValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_bottom);

        con = this;
        settingValue = SettingValue.getInstance(con);
        settingValue.loadSettingValue();

        container = findViewById(R.id.container);
        btnMenu = findViewById(R.id.btnMenu);
        btn_menu1 = findViewById(R.id.btn_menu1);
        btn_menu2 = findViewById(R.id.btn_menu2);
        btn_menu3 = findViewById(R.id.btn_menu3);
        btn_menu4 = findViewById(R.id.btn_menu4);
        btn_menu5 = findViewById(R.id.btn_menu5);

        btn_menu2.setOnClickListener(this);
        btn_menu3.setOnClickListener(this);
        btn_menu4.setOnClickListener(this);
        btn_menu5.setOnClickListener(this);

        if(isFromEditing){

            isFromEditing = false;
            updateMenu(2);
            container.setBackground(null);
            Fragment fragment = new FragmentCart();
            replaceFragment(fragment);
            btnMenu.setVisibility(View.GONE);

        }else{

            Fragment fragment = new FragmentHome();
            replaceFragment(fragment);
        }
    }

    @Override
    public void onClick(View v) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        switch (v.getId()){
            case R.id.btn_menu2:
                updateMenu(2);
                container.setBackground(null);
                Fragment fragment = new FragmentCart();
                replaceFragment(fragment);
                btnMenu.setVisibility(View.GONE);
                break;
            case R.id.btn_menu3:
                updateMenu(3);
                container.setBackground(null);
                fragment = new NewHistoryFragment();
                fragmentManager.beginTransaction().replace(R.id.MainFrame, fragment).commit();
                btnMenu.setVisibility(View.GONE);
                break;
            case R.id.btn_menu4:
                updateMenu(4);
                container.setBackground(getResources().getDrawable(R.drawable.background));
                fragment = new FragmentSetting();
                fragmentManager.beginTransaction().replace(R.id.MainFrame, fragment).commit();
                btnMenu.setVisibility(View.GONE);
                break;
            case R.id.btn_menu5:
                container.setBackground(getResources().getDrawable(R.drawable.background));
                updateMenu(1);
                fragment = new FragmentHome();
                fragmentManager.beginTransaction().replace(R.id.MainFrame, fragment).commit();
                btnMenu.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void editOpenClick(View v){
//        SettingValue.isNewImage = true;
        SettingValue.isEditFrom = "gallery";
        startActivity(new Intent(NewBottomActivity.this, PhotoEditingActivity.class));
    }

    public void logoutClick(View v){
        settingValue.saveSetting(settingValue.str_isLogin, false);
        startActivity(new Intent(NewBottomActivity.this, LoginActivity.class));
        finish();
    }

    void updateMenu(int i){
        switch (i){
            case 1:
                btn_menu1.setImageDrawable(getResources().getDrawable(R.drawable.bottom_icon1));
                btn_menu2.setImageDrawable(getResources().getDrawable(R.drawable.bottom1_icon2));
                btn_menu3.setImageDrawable(getResources().getDrawable(R.drawable.bottom1_icon3));
                btn_menu4.setImageDrawable(getResources().getDrawable(R.drawable.bottom1_icon4));
                break;
            case 2:
                btn_menu1.setImageDrawable(getResources().getDrawable(R.drawable.bottom1_icon1));
                btn_menu2.setImageDrawable(getResources().getDrawable(R.drawable.bottom_icon2));
                btn_menu3.setImageDrawable(getResources().getDrawable(R.drawable.bottom1_icon3));
                btn_menu4.setImageDrawable(getResources().getDrawable(R.drawable.bottom1_icon4));
                break;
            case 3:
                btn_menu1.setImageDrawable(getResources().getDrawable(R.drawable.bottom1_icon1));
                btn_menu2.setImageDrawable(getResources().getDrawable(R.drawable.bottom1_icon2));
                btn_menu3.setImageDrawable(getResources().getDrawable(R.drawable.bottom_icon3));
                btn_menu4.setImageDrawable(getResources().getDrawable(R.drawable.bottom1_icon4));
                break;
            case 4:
                btn_menu1.setImageDrawable(getResources().getDrawable(R.drawable.bottom1_icon1));
                btn_menu2.setImageDrawable(getResources().getDrawable(R.drawable.bottom1_icon2));
                btn_menu3.setImageDrawable(getResources().getDrawable(R.drawable.bottom1_icon3));
                btn_menu4.setImageDrawable(getResources().getDrawable(R.drawable.bottom_icon4));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(NewBottomActivity.this);
        myAlertDialog.setMessage("Er du sikker på at du vil lukke appen?");
        myAlertDialog.setTitle(getResources().getString(R.string.app_name));
        myAlertDialog.setIcon(R.drawable.logo);
        myAlertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface arg0, int arg1) {
                finish();
                System.exit(0);
            }
        });

        myAlertDialog.setNegativeButton("Fortryd", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });

        myAlertDialog.show();
    }
}
