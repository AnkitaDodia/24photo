package com.advait.photobook.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.advait.photobook.ExpandableGrid.Orderline;
import com.advait.photobook.Models.OrderEntry;
import com.advait.photobook.Models.SingleUpload;
import com.advait.photobook.R;
import com.advait.photobook.Utils.Api;
import com.advait.photobook.Utils.AppController;
import com.advait.photobook.Utils.ImageUtil;
import com.advait.photobook.Utils.ImagesCache;
import com.advait.photobook.Utils.InternetStatus;
import com.advait.photobook.Utils.SettingValue;
import com.advait.photobook.common.BaseActivity;
import com.advait.photobook.restinterface.RestInterface;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.google.gson.Gson;
import com.suke.widget.SwitchButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;


public class PhotoEditingActivity extends BaseActivity {

    Context mContext;

    ImageView image;
    SwitchButton switch_button, switch_button1;
    TextView tvCount, tvSize, btnCrop, tvOne, txt_next_step;

    public static final int CROP_REQUEST_CODE = 1;

    SettingValue settingValue;

    int count = 1, photoIndex = 0;
    ArrayList<Image> images;
    ArrayList<String> LargeImages, NormalImages;
    Uri selectedImageUri;

    Bitmap EditedBitmap;
    ColorMatrix AdjustmentMatrix;


    HashMap<String, String> ImageQTY;
    HashMap<String, String> Black_White;
    HashMap<String, String> White_Frame;
    HashMap<String, String> mOriginalBase64;
    HashMap<String, String> mEditedBase64;

    private ImagesCache cache;

    private boolean isBorder = false, isGray = false;

    public static int temp = 0;
    LinearLayout ll_progress;

    ProgressBar progress_image;

    String rawPhoto, productionPhoto, orderEntryId;
    boolean isInternet;
    static ProgressDialog mProgressDialog;

    String original, converted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mContext = this;
        isInternet = new InternetStatus().isInternetOn(mContext);


        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Billeder uploades, vent venligst..");
        mProgressDialog.setCancelable(false);

        settingValue = SettingValue.getInstance(mContext);
        settingValue.loadSettingValue();

        initviews();
        setClickListners();

        cache = ImagesCache.getInstance();
        cache.initializeCache();

        ImageQTY = new HashMap<String, String>();
        Black_White = new HashMap<String, String>();
        White_Frame = new HashMap<String, String>();
        mEditedBase64 = new HashMap<String, String>();
        mOriginalBase64 = new HashMap<String, String>();

        AdjustmentMatrix = new ColorMatrix();

        if (SettingValue.isEditFrom.equalsIgnoreCase("gallery")) {
            images = new ArrayList<Image>();
            PickMultipleImages();
        } else if (SettingValue.isEditFrom.equalsIgnoreCase("cart")) {
            txt_next_step.setBackgroundResource(R.drawable.roundcorner_dark);
            OrderEntry data = BaseActivity.orderEntry;
            setCropedImage(data);
        } else if (SettingValue.isEditFrom.equalsIgnoreCase("history")) {
            //TODO do your job

            txt_next_step.setBackgroundResource(R.drawable.roundcorner_dark);
            Orderline data = BaseActivity.mOrderlineHistory;
            setCropedImage(data);
        }

    }

    private void PickMultipleImages() {
        Intent intent = new Intent(mContext, AlbumSelectActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 24);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    private void initviews() {

        image = findViewById(R.id.image);

        progress_image = findViewById(R.id.progress_image);

        tvOne = findViewById(R.id.lblOne_main2);
        tvCount = findViewById(R.id.tvCount);
        tvSize = findViewById(R.id.tvSize);
        switch_button = findViewById(R.id.switch_button);
        switch_button1 = findViewById(R.id.switch_button1);
        btnCrop = findViewById(R.id.btnCrop);

        txt_next_step = findViewById(R.id.txt_next_step);
        ll_progress = findViewById(R.id.ll_progress);
    }

    private void setClickListners() {
        switch_button.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {

                if (isChecked) {
                    isGray = true;
                    AdjustmentMatrix = new ColorMatrix();
                    AdjustmentMatrix.setSaturation(0);

                    ColorMatrixColorFilter filter = new ColorMatrixColorFilter(AdjustmentMatrix);
                    image.setColorFilter(filter);
                } else {
                    isGray = false;
                    image.setColorFilter(null);
                }
            }
        });

        switch_button1.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {

                if (isChecked) {

                    isBorder = true;
                    try {
                        image.setImageBitmap(addWhiteBorder(OriginalBitmap));
                    } catch (Exception e) {

                    }

                } else {
                    isBorder = false;
                    image.setImageBitmap(OriginalBitmap);
                }
            }
        });
    }

    private void ResetAllView() {
        if (SettingValue.isBlackWhite) {
            switch_button.setChecked(true);
            isGray = true;
        } else {
            switch_button.setChecked(false);
            isGray = false;
        }

        if (SettingValue.isFrame) {
            switch_button1.setChecked(true);
            isBorder = true;
        } else {
            switch_button1.setChecked(false);
            isBorder = false;
        }

        tvCount.setText("1");
        count = 1;
    }

    private class AsyncPutBitmap extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            StartProgressBar();
            doSomethingWhenProgressNotShown(true);

            cache.addImageToWarehouse(String.valueOf(photoIndex), GetProcessedBitmap());
            ImageQTY.put(String.valueOf(photoIndex), tvCount.getText().toString());
            Black_White.put(String.valueOf(photoIndex), Boolean.toString(isGray));
            White_Frame.put(String.valueOf(photoIndex), Boolean.toString(isBorder));
        }

        @Override
        protected String doInBackground(String... params) {

            String str = "Ankita";
            System.out.println("doInBackground");



            try {
                mOriginalBase64.put(String.valueOf(photoIndex), ImageUtil.convert(decodeFile(new File(images.get(photoIndex).path))));
                mEditedBase64.put(String.valueOf(photoIndex), ImageUtil.convert(cache.getImageFromWarehouse(String.valueOf(photoIndex))));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return str;
        }

        @Override
        protected void onPostExecute(String str) {

            System.out.println("str" + str);

            Log.e("nextClick", "photoIndex  : " + photoIndex);
            Log.e("nextClick", "images size  : " + LargeImages.size());

            if (photoIndex >= (LargeImages.size() - 2)) {
                txt_next_step.setBackgroundResource(R.drawable.roundcorner_dark);
            }

            if (photoIndex >= (LargeImages.size() - 1)) {

                if (isInternet) {
                    for (int j = 0; j <= (LargeImages.size() - 1); j++) {
                        try {
                            Log.e("J Value", "" + j);
                            SingleUpload(ImageQTY.get(String.valueOf(j)), Black_White.get(String.valueOf(j)), White_Frame.get(String.valueOf(j)), mOriginalBase64.get(String.valueOf(j)), mEditedBase64.get(String.valueOf(j)));
                        } catch (Exception e) {
                            Log.e("Exception", "Exception : " + e);
                        }
                    }
                } else {
                    Toast.makeText(mContext, "Ingen internetforbindelse", Toast.LENGTH_LONG).show();
                }

                return;
            }

            ResetAllView();
            photoIndex++;
            setCropedImage();

            doSomethingWhenProgressNotShown(false);
        }
    }

    public void nextClick(View v) {
        if (SettingValue.isEditFrom.equalsIgnoreCase("gallery")) {
            try {

                AsyncPutBitmap asyncPutBitmap = new AsyncPutBitmap();
                asyncPutBitmap.execute();

            } catch (Exception e) {

                Log.e("Exception", "Exception : " + e);

            }
        } else if (SettingValue.isEditFrom.equalsIgnoreCase("cart")) {
            try {

                String qty = tvCount.getText().toString();
                String bAndWFlag = Boolean.toString(isGray);
                String borderFlag = Boolean.toString(isBorder);

                if (isInternet) {

                    SingleUpload(qty, bAndWFlag, borderFlag, "", ImageUtil.convert(GetProcessedBitmap()));
                } else {

                    Toast.makeText(mContext, "Ingen internetforbindelse", Toast.LENGTH_LONG).show();

                }

            } catch (Exception e) {
                Log.e("Exception", "Exception : " + e);
            }
        } else if (SettingValue.isEditFrom.equalsIgnoreCase("history")) {
            //TODO do your job

            try {

                String qty = tvCount.getText().toString();
                String bAndWFlag = Boolean.toString(isGray);
                String borderFlag = Boolean.toString(isBorder);

                if (isInternet) {

                    SingleUpload(qty, bAndWFlag, borderFlag, ImageUtil.convert(OriginalBitmap), ImageUtil.convert(GetProcessedBitmap()));
                } else {
                    Toast.makeText(mContext, "Ingen internetforbindelse", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                Log.e("Exception", "Exception : " + e);
            }
        }

    }

    public void CheckImageSize() {

        LargeImages = new ArrayList<>();
        NormalImages = new ArrayList<>();

        for (int j = 0; j <= (images.size() - 1); j++) {

            try {

//                Uri mImageUri = Uri.fromFile(new File(images.get(j).path));
//                Bitmap tempBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageUri);
                Bitmap tempBitmap = decodeFile(new File(images.get(j).path));

                if (GetRatioofImage(tempBitmap.getWidth(), tempBitmap.getHeight())) {

                    LargeImages.add(images.get(j).path);

                } else {

                    NormalImages.add(images.get(j).path);
                }

            } catch (Exception e) {
                Log.e("Exception b", "Exception b : " + e);
            }
        }

        Log.e("LargeImages", "Images Size : " + LargeImages.size());
        Log.e("NormalImages", "Images Size : " + NormalImages.size());


        if(NormalImages.size() == 0){

            cache.clearCache();

            ImageQTY.clear();
            Black_White.clear();
            White_Frame.clear();
            mEditedBase64.clear();
            mOriginalBase64.clear();

            AdjustmentMatrix = new ColorMatrix();



            if (LargeImages.size() == 0) {

                isFromEditing = true;
                startActivity(new Intent(mContext, NewBottomActivity.class));
                finish();

                return;
            }

            if (LargeImages.size() == 1) {

                txt_next_step.setBackgroundResource(R.drawable.roundcorner_dark);
            }

            setCropedImage();

        }else{


            tvOne.setText("Foto " + "1" + " af " + images.size());

//            AsyncPutBitmapNormal asyncPutBitmapn = new AsyncPutBitmapNormal();
//            asyncPutBitmapn.execute();

            normalImageUpload();
        }
    }

    /*************************************************************************************************************/

    private void normalImageUpload()
    {
        if (isInternet) {

            PostMultipleImagesTask mTask = new PostMultipleImagesTask();
            mTask.execute();


        } else {

            Toast.makeText(mContext, "Ingen internetforbindelse", Toast.LENGTH_LONG).show();
        }
    }

    private class PostMultipleImagesTask extends AsyncTask<String, Void, SingleUpload> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            doSomethingWhenProgressNotShown(true);
        }

        @Override
        protected SingleUpload doInBackground(String... recipeIds) {


            SingleUpload p = null;
            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            for (int k = 0; k <= (NormalImages.size() - 1); k++) {


                    try {

//                        MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), Uri.fromFile(new File(NormalImages.get(k))))
                        original = ImageUtil.convert(decodeFile(new File(images.get(k).path)));
                        converted =  ImageUtil.convert(GetProcessedBitmapNormal(NormalImages.get(k)));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                JSONObject jsonObject = new JSONObject();

                try {

                    Log.e("values of imageqty", "value : " + "1");
                    Log.e("values of black_white", "black_white : " + Boolean.toString(settingValue.isBlackWhite));
                    Log.e("values of white_frame", "white_frame : " + Boolean.toString(settingValue.isFrame));
                    Log.e("values of OriBase64", "value : " + original);
                    Log.e("values of EditedBase64", "value : " + converted);
                    Log.e("values of user_id", "value : " + settingValue.UserId);
                    Log.e("values of printsize", "value : " + ImageSize);

                    jsonObject.put("user_id", settingValue.UserId);//userid
                    jsonObject.put("qty", "1");
                    jsonObject.put("black_white", Boolean.toString(settingValue.isBlackWhite));
                    jsonObject.put("white_frame", Boolean.toString(settingValue.isFrame));
                    jsonObject.put("imageprintsize", ImageSize);
                    jsonObject.put("link_image_raw", original);
                    jsonObject.put("link_image_production", converted);


                    Log.e("jsonObject", "" + jsonObject.toString());

                    TypedInput mTypedInput = new TypedByteArray("application/json", jsonObject.toString().getBytes("UTF-8"));
                    p = restInterface.getAddtoBasketRequest(mTypedInput);

                } catch (Exception e) {
                    Log.e("JSONObject Here", e.toString());
                }


            }
            return p;
        }

        @Override
        protected void onPostExecute(SingleUpload result) {
            // response here
            doSomethingWhenProgressNotShown(false);
            Log.e("SingleUpload","SingleUpload :"+result.getText());

            Toast.makeText(getApplication(), "Billedet er gemt korrekt", Toast.LENGTH_SHORT).show();

            doSomethingWhenProgressNotShown(false);

            cache.clearCache();

            ImageQTY.clear();
            Black_White.clear();
            White_Frame.clear();
            mEditedBase64.clear();
            mOriginalBase64.clear();

            AdjustmentMatrix = new ColorMatrix();

            if (LargeImages.size() == 0) {

                isFromEditing = true;
                startActivity(new Intent(mContext, NewBottomActivity.class));
                finish();

                return;
            }

            if (LargeImages.size() == 1) {

                txt_next_step.setBackgroundResource(R.drawable.roundcorner_dark);
            }

            setCropedImage();
        }

    }

    private void setCropedImage() {
        try {

            photoIndex++;
            tvOne.setText("Foto " + photoIndex + " af " + LargeImages.size());
            photoIndex--;

            selectedImageUri = Uri.fromFile(new File(LargeImages.get(photoIndex)));

            Log.e("URI", "" + selectedImageUri);


//            OriginalBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);

            OriginalBitmap = decodeFile(new File(images.get(photoIndex).path));

            image.setImageBitmap(OriginalBitmap);

            if (settingValue.isBlackWhite) {

                AdjustmentMatrix.setSaturation(0);

                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(AdjustmentMatrix);
                image.setColorFilter(filter);
            } else {
                image.setColorFilter(null);
            }

            if (settingValue.isFrame) {
                image.setImageBitmap(addWhiteBorder(OriginalBitmap));
            } else {
                image.setImageBitmap(OriginalBitmap);
            }

            if (GetRatioofImage(OriginalBitmap.getWidth(), OriginalBitmap.getHeight())) {

                showCropDialog();
            } else {

            }
        } catch (Exception e) {

        }
    }

    private void setCropedImage(OrderEntry orderEntry) {// this method is used when user come from cart
        try {

            tvOne.setText("Foto 1 af 1");
            tvCount.setText("" + orderEntry.getQty());
            count = Integer.parseInt(orderEntry.getQty());

            rawPhoto = orderEntry.getLinkImageRaw();
            productionPhoto = orderEntry.getLinkImageProduction();
            orderEntryId = orderEntry.getOrderEntriesId();
            ImageSize = orderEntry.getImageprintsize();


            tvSize.setText(ImageSize);

            Glide.with(this).load(orderEntry.getLinkImageRaw()).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    progress_image.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    progress_image.setVisibility(View.GONE);
                    return false;
                }
            }).into(image);

            AsyncGettingBitmapFromUrl asyncGettingBitmapFromUrl = new AsyncGettingBitmapFromUrl();
            asyncGettingBitmapFromUrl.execute();

            if (settingValue.isBlackWhite) {

                AdjustmentMatrix.setSaturation(0);

                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(AdjustmentMatrix);
                image.setColorFilter(filter);
            } else {
                image.setColorFilter(null);
            }

            if (settingValue.isFrame) {
                image.setImageBitmap(addWhiteBorder(OriginalBitmap));
            } else {
                image.setImageBitmap(OriginalBitmap);
            }
        } catch (Exception e) {

        }
    }

    private void setCropedImage(Orderline mOrderHistory) {// this method is used when user come from History
        try {

            tvOne.setText("Foto 1 af 1");
            tvCount.setText("" + mOrderHistory.getQty());
            count = Integer.parseInt(mOrderHistory.getQty());

            rawPhoto = mOrderHistory.getLinkImageRaw();
            productionPhoto = mOrderHistory.getLinkImageProduction();
//            orderEntryId = mOrderHistory.getOrderEntriesId();
            ImageSize = mOrderHistory.getImageprintsize();


            tvSize.setText(ImageSize);

            Glide.with(this).load(mOrderHistory.getLinkImageRaw()).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    progress_image.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    progress_image.setVisibility(View.GONE);
                    return false;
                }
            }).into(image);

            AsyncGettingBitmapFromUrl asyncGettingBitmapFromUrl = new AsyncGettingBitmapFromUrl();
            asyncGettingBitmapFromUrl.execute();

            if (settingValue.isBlackWhite) {

                AdjustmentMatrix.setSaturation(0);

                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(AdjustmentMatrix);
                image.setColorFilter(filter);
            } else {
                image.setColorFilter(null);
            }

            if (settingValue.isFrame) {
                image.setImageBitmap(addWhiteBorder(OriginalBitmap));
            } else {
                image.setImageBitmap(OriginalBitmap);
            }
        } catch (Exception e) {

        }
    }

    private class AsyncGettingBitmapFromUrl extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            doSomethingWhenProgressNotShown(true);
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            System.out.println("doInBackground");

            Bitmap bitmap = null;

            if (SettingValue.isEditFrom.equalsIgnoreCase("cart")) {

                bitmap = getBitmapFromURL(orderEntry.getLinkImageRaw());

            } else if (SettingValue.isEditFrom.equalsIgnoreCase("history")) {

                bitmap = getBitmapFromURL(BaseActivity.mOrderlineHistory.getLinkImageRaw());
            }

            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap1) {

            System.out.println("bitmap" + bitmap1);

            OriginalBitmap = bitmap1;

            if (SettingValue.isEditFrom.equalsIgnoreCase("cart")) {

                if (orderEntry.getBlackWhite().equalsIgnoreCase("1")) {
                    switch_button.setChecked(true);
                    isGray = true;

                    AdjustmentMatrix.setSaturation(0);

                    ColorMatrixColorFilter filter = new ColorMatrixColorFilter(AdjustmentMatrix);
                    image.setColorFilter(filter);
                } else {

                    switch_button.setChecked(false);
                    isGray = false;

                    image.setColorFilter(null);
                }

                if (orderEntry.getWhiteFrame().equalsIgnoreCase("1")) {

                    switch_button1.setChecked(true);
                    isBorder = true;

                    image.setImageBitmap(addWhiteBorder(bitmap1));
                } else {

                    switch_button1.setChecked(false);
                    isBorder = false;
                    image.setImageBitmap(bitmap1);
                }

                if (GetRatioofImage(OriginalBitmap.getWidth(), OriginalBitmap.getHeight())) {

                    showCropDialog();
                } else {
                }

                Log.e("FROM", "BITMAP METHOD");
            } else if (SettingValue.isEditFrom.equalsIgnoreCase("history")) {


                if (BaseActivity.mOrderlineHistory.getBlackWhite().equalsIgnoreCase("1")) {
                    switch_button.setChecked(true);
                    isGray = true;

                    AdjustmentMatrix.setSaturation(0);

                    ColorMatrixColorFilter filter = new ColorMatrixColorFilter(AdjustmentMatrix);
                    image.setColorFilter(filter);
                } else {

                    switch_button.setChecked(false);
                    isGray = false;

                    image.setColorFilter(null);
                }

                if (BaseActivity.mOrderlineHistory.getWhiteFrame().equalsIgnoreCase("1")) {

                    switch_button1.setChecked(true);
                    isBorder = true;

                    image.setImageBitmap(addWhiteBorder(bitmap1));
                } else {

                    switch_button1.setChecked(false);
                    isBorder = false;
                    image.setImageBitmap(bitmap1);
                }

                if (GetRatioofImage(OriginalBitmap.getWidth(), OriginalBitmap.getHeight())) {

                    showCropDialog();
                } else {
                }

                Log.e("FROM", "BITMAP METHOD");
            }


            doSomethingWhenProgressNotShown(false);
        }
    }

    public Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public Bitmap ApplyColorMatrix(Bitmap bmp_original) {

        Bitmap bmp_edited = bmp_original.copy(Bitmap.Config.RGB_565, true);
        Canvas mCanvas = new Canvas(bmp_edited);
        Paint mPaint = new Paint();

        ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(AdjustmentMatrix);

        mPaint.setColorFilter(colorFilter);

        mCanvas.drawBitmap(bmp_original, 0, 0, mPaint);

        return bmp_edited;
    }

    private Bitmap GetProcessedBitmapNormal(String path) {

        Log.e("GetProcessed Normal", "isGray :  " + settingValue.isBlackWhite);
        Log.e("GetProcessed Normal", "isBorder :  " + settingValue.isFrame);

        try {

//            Uri mImageUri = Uri.fromFile(new File(path));
//            OriginalBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageUri);
            OriginalBitmap = decodeFile(new File(path));

            EditedBitmap = OriginalBitmap;

        } catch (Exception e) {

        }


        if (settingValue.isBlackWhite) {

            AdjustmentMatrix.setSaturation(0);
            EditedBitmap = ApplyColorMatrix(OriginalBitmap);

        } else {

            AdjustmentMatrix = new ColorMatrix();
            EditedBitmap = ApplyColorMatrix(OriginalBitmap);
        }

        if (settingValue.isFrame) {

            EditedBitmap = addWhiteBorder(EditedBitmap);

        } else {


        }


        return EditedBitmap;
    }

    private Bitmap GetProcessedBitmap() {
        Log.e("GetProcessedBitmap", "isGray :  " + isGray);
        Log.e("GetProcessedBitmap", "isBorder :  " + isBorder);

        EditedBitmap = OriginalBitmap;


        if (isGray) {

            EditedBitmap = ApplyColorMatrix(OriginalBitmap);
        }

        if (isBorder) {

            EditedBitmap = addWhiteBorder(EditedBitmap);
        }

        return EditedBitmap;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == Constants.REQUEST_CODE && resultCode == RESULT_OK && data != null) {
                images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);


                CheckImageSize();


            } else if (requestCode == CROP_REQUEST_CODE && resultCode == RESULT_OK) {

                tvSize.setText(ImageSize);
                if (isBorder) {

                    image.setImageBitmap(addWhiteBorder(OriginalBitmap));

                } else {

                    image.setImageBitmap(OriginalBitmap);
                }

                if (GetRatioofImage(OriginalBitmap.getWidth(), OriginalBitmap.getHeight())) {

                    showCropDialog();
                } else {

                }

            } else {
                Toast.makeText(this, "Du har ikke valgt nogle billeder", Toast.LENGTH_LONG).show();
                onBackPressed();
            }

        } catch (Exception e) {
            Toast.makeText(this, "Noget gik galt. Prøv igen senere", Toast.LENGTH_LONG).show();
            onBackPressed();
        }
    }

    private Bitmap addWhiteBorder(Bitmap bmp) {

        int borderSize = CalclulateBorderSize(bmp);

        Bitmap bmpWithBorder = Bitmap.createBitmap(bmp.getWidth() + borderSize * 2,
                bmp.getHeight() + borderSize * 2, bmp.getConfig());

        Canvas canvas = new Canvas(bmpWithBorder);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bmp, borderSize, borderSize, null);
        return bmpWithBorder;
    }

    public boolean GetRatioofImage(int width, int height) {

        boolean flag;
        Log.e("Actual size", "intial newwidth : " + width + "   intial newheight : " + height);
        double newheight, newwidth;

        if (height > width) {

            newheight = (double) height / (double) width;
            newheight = newheight * 10;
            newwidth = 10;

            if (newheight > Integer.parseInt(SettingValue.MaxPhotoSize)) {

//                showcrop dialog
                flag = true;
            } else {
                flag = false;
            }

        } else {

            newwidth = (double) width / (double) height;
            newwidth = newwidth * 10;
            newheight = 10;

            if (newwidth > Integer.parseInt(SettingValue.MaxPhotoSize)) {
//                showcrop dialog
                flag = true;
            } else {
                flag = false;
            }
        }

        Log.e("Actual size", "api output newwidth : " + newwidth + "  newheight : " + newheight);
        NumberFormat formatter = NumberFormat.getNumberInstance();
        formatter.setMinimumFractionDigits(0);
        formatter.setMaximumFractionDigits(0);

        ImageSize = " " + formatter.format(newwidth) + "  *  " + formatter.format(newheight) + " cm";
        tvSize.setText(ImageSize);
        return flag;
    }

    private void showCropDialog() {
        new AlertDialog.Builder(mContext)
                .setTitle("24fotos")
                .setMessage("Beskær fotoet" +
                        "\nFotoet er mere end "+ SettingValue.MaxPhotoSize +"cm. på den  lange side, beskær det venligst.")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("MainActivity", "Sending atomic bombs to Jupiter");

                        if (dialog != null) {
                            dialog.dismiss();
                        }

                        Intent intent_crop = new Intent(mContext, CropActivity.class);
                        startActivityForResult(intent_crop, CROP_REQUEST_CODE);
                    }
                })
                .show();
    }

    private void SingleUpload(String qty, String black_white, String white_frame, String OriginalBase64, String EditedBase64) {
        doSomethingWhenProgressNotShown(true);

//        String urlUpload = "https://36fotos.dk/API/webservices/addtobasketnewsingle.php";
        String urlUpload = "https://24fotos.dk/API/webservices/addtobasketnewsingle.php";

        JSONObject jsonObject = new JSONObject();

        try {

            if (SettingValue.isEditFrom.equalsIgnoreCase("gallery")) {
                Log.e("values of imageqty", "value : " + qty);
                Log.e("values of black_white", "black_white : " + black_white);
                Log.e("values of white_frame", "white_frame : " + white_frame);
                Log.e("values of OriBase64", "value : " + OriginalBase64);
                Log.e("values of EditedBase64", "value : " + EditedBase64);
                Log.e("values of user_id", "value : " + settingValue.UserId);
                Log.e("values of printsize", "value : " + ImageSize);

                jsonObject.put("user_id", settingValue.UserId);//userid
                jsonObject.put("qty", qty);
                jsonObject.put("black_white", black_white);
                jsonObject.put("white_frame", white_frame);
                jsonObject.put("imageprintsize", ImageSize.replace(" ",""));
                jsonObject.put("link_image_raw", OriginalBase64);
                jsonObject.put("link_image_production", EditedBase64);

            } else if (SettingValue.isEditFrom.equalsIgnoreCase("cart")) {
                Log.e("userid", "" + settingValue.UserId);
                Log.e("qty", "" + qty);
                Log.e("black_white", "" + black_white);
                Log.e("white_frame", "" + white_frame);
                Log.e("Html.fromHtml(rawPhoto)", "" + Html.fromHtml(rawPhoto));
                Log.e("Html(productionPhoto)", "" + Html.fromHtml(productionPhoto));
                Log.e("orderEntryId", "" + orderEntryId);
                Log.e("is_pro_update", "" + EditedBase64);
                Log.e("imageprintsize", "" + ImageSize);

                jsonObject.put("user_id", settingValue.UserId);
                jsonObject.put("qty", qty);
                jsonObject.put("black_white", black_white);
                jsonObject.put("white_frame", white_frame);
                jsonObject.put("imageprintsize", ImageSize.replace(" ",""));
                jsonObject.put("link_image_raw", Html.fromHtml(rawPhoto));
                jsonObject.put("link_image_production", Html.fromHtml(productionPhoto));
                jsonObject.put("order_entries_id", orderEntryId);
                jsonObject.put("is_raw_update", "");
                jsonObject.put("is_pro_update", EditedBase64);

                Log.e("jsonObject", "" + jsonObject.toString());
            } else if (SettingValue.isEditFrom.equalsIgnoreCase("history")) {
                //TODO do your job

                Log.e("values of imageqty", "value : " + qty);
                Log.e("values of black_white", "black_white : " + black_white);
                Log.e("values of white_frame", "white_frame : " + white_frame);
                Log.e("values of OriBase64", "value : " + OriginalBase64);
                Log.e("values of EditedBase64", "value : " + EditedBase64);


                jsonObject.put("user_id", settingValue.UserId);//userid
                jsonObject.put("qty", qty);
                jsonObject.put("black_white", black_white);
                jsonObject.put("white_frame", white_frame);
                jsonObject.put("imageprintsize", ImageSize.replace(" ",""));
                jsonObject.put("link_image_raw", OriginalBase64);
                jsonObject.put("link_image_production", EditedBase64);
            }

        } catch (JSONException e) {
            Log.e("JSONObject Here", e.toString());
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, urlUpload, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("Message response", jsonObject.toString());

                        if (SettingValue.isEditFrom.equalsIgnoreCase("gallery")) {
                            Log.e("Message from server", "temp    : " + temp + "   image size : " + LargeImages.size());

                            if (temp == (LargeImages.size() - 1)) {
                                Toast.makeText(getApplication(), "Billedet er gemt korrekt", Toast.LENGTH_SHORT).show();

                                doSomethingWhenProgressNotShown(false);

                                isFromEditing = true;
                                startActivity(new Intent(mContext, NewBottomActivity.class));
                                finish();
                            }
                            temp++;
                        } else if (SettingValue.isEditFrom.equalsIgnoreCase("cart")) {
                            Toast.makeText(getApplication(), "Billedet er gemt korrekt", Toast.LENGTH_SHORT).show();

                            doSomethingWhenProgressNotShown(false);

                            isFromEditing = true;
                            startActivity(new Intent(mContext, NewBottomActivity.class));
                            finish();
                        } else if (SettingValue.isEditFrom.equalsIgnoreCase("history")) {
                            //TODO do your job

                            Toast.makeText(getApplication(), "Billedet er gemt korrekt", Toast.LENGTH_SHORT).show();

                            doSomethingWhenProgressNotShown(false);

                            isFromEditing = true;
                            startActivity(new Intent(mContext, NewBottomActivity.class));
                            finish();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("Message from server", volleyError.toString());
                Toast.makeText(getApplication(), "Error Occurred", Toast.LENGTH_SHORT).show();

                doSomethingWhenProgressNotShown(false);
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(200 * 30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }

    public void plusClick(View v) {
        count++;
        tvCount.setText(count + "");
    }

    public void minusClick(View v) {
        if (count > 0)
            count--;
        tvCount.setText(count + "");
    }

    public int CalclulateBorderSize(Bitmap mOriBitmap) {

        Double BorderSize;

        int width = mOriBitmap.getWidth();
        int height = mOriBitmap.getHeight();

        Log.v("Pictures", "Width and height are " + width + "--" + height);

        if (width > height) {
            // landscape
            BorderSize = height / 12.5;

        } else if (height > width) {
            // portrait

            BorderSize = width / 12.5;
        } else {
            // square
            BorderSize = width / 12.5;
        }

        return Integer.valueOf(BorderSize.intValue());
    }

    public void cropImageClick(View view) {

        Intent intent_crop = new Intent(mContext, CropActivity.class);
        startActivityForResult(intent_crop, CROP_REQUEST_CODE);
    }

    public void removeClick(View view) {
        if (SettingValue.isEditFrom.equalsIgnoreCase("gallery")) {
            //call normal delete
            if (!ImageQTY.isEmpty()) {
                ImageQTY.remove(String.valueOf(photoIndex));
            }

            if (!Black_White.isEmpty()) {
                Black_White.remove(String.valueOf(photoIndex));
            }

            if (!White_Frame.isEmpty()) {
                White_Frame.remove(String.valueOf(photoIndex));
            }

            if (!mOriginalBase64.isEmpty()) {
                mOriginalBase64.remove(String.valueOf(photoIndex));
            }

            if (!mEditedBase64.isEmpty()) {
                mEditedBase64.remove(String.valueOf(photoIndex));
            }

            if (LargeImages.size() > 0) {
                Log.e("delete", "photoIndex Before: " + photoIndex);
                Log.e("delete", "images.size() Before: " + LargeImages.size());

                LargeImages.remove(photoIndex);

                Log.e("delete", "photoIndex: " + photoIndex);
                Log.e("delete", "images.size(): " + LargeImages.size());

                if (photoIndex == LargeImages.size()) {
                    txt_next_step.setBackgroundResource(R.drawable.roundcorner_dark);
                    photoIndex--;
                    setCropedImage();
                } else {
                    setCropedImage();
                }
            } else {
                startActivity(new Intent(PhotoEditingActivity.this, NewBottomActivity.class));
                finish();
            }
        } else if (SettingValue.isEditFrom.equalsIgnoreCase("cart")) {
            //call delete api
            if (isInternet) {
                sendDeleteApiRequest();
            } else {
                Toast.makeText(PhotoEditingActivity.this, "Ingen internetforbindelse", Toast.LENGTH_LONG).show();
            }
        } else if (SettingValue.isEditFrom.equalsIgnoreCase("history")) {
            //TODO do your job
            isFromEditing = false;
            startActivity(new Intent(PhotoEditingActivity.this, NewBottomActivity.class));
            finish();
        }
    }

    private void sendDeleteApiRequest() {
        String tag_json_obj = "json_obj_req";
        doSomethingWhenProgressNotShown(true);

        String url = Api.baseurl + Api.delete + SettingValue.UserId + "&order_entries_id=" + orderEntryId;
        Log.e("DELETE_URL", "" + url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("DELETE", "==> " + response);

                        try {
                            JSONObject data = new JSONObject(response);
                            switch (data.getString("text")) {
                                case "OrderOk":
                                    Toast.makeText(PhotoEditingActivity.this, "Billedet er fjernet", Toast.LENGTH_SHORT).show();
                                    isFromEditing = true;
                                    startActivity(new Intent(mContext, NewBottomActivity.class));
                                    finish();
                                    break;
                            }
                        } catch (JSONException e) {
                            doSomethingWhenProgressNotShown(false);
                            Toast.makeText(PhotoEditingActivity.this, "Noget gik galt. Prøv igen senere", Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(PhotoEditingActivity.this, "Noget gik galt. Prøv igen senere", Toast.LENGTH_LONG).show();
                        }
                        doSomethingWhenProgressNotShown(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        doSomethingWhenProgressNotShown(false);
                        Toast.makeText(PhotoEditingActivity.this, "Noget gik galt. Prøv igen senere", Toast.LENGTH_LONG).show();
                    }
                }) {
        };

        AppController.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
    }

    public static void doSomethingWhenProgressNotShown(boolean status) {
        if (mProgressDialog.isShowing() && status) {
            mProgressDialog.dismiss();
            mProgressDialog.show();
        } else if (status) {
            mProgressDialog.show();
        } else {
            mProgressDialog.dismiss();
        }
    }
}