package com.advait.photobook.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.advait.photobook.R;
import com.advait.photobook.Utils.Api;
import com.advait.photobook.Utils.AppController;
import com.advait.photobook.Utils.SettingValue;
import com.advait.photobook.common.BaseActivity;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class ResetPasswordActivity extends BaseActivity implements View.OnClickListener {

    EditText txtUsername;
    TextView btnReset,btnBack;

    Context con;
    SettingValue settingValue;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        con = this;
        settingValue = SettingValue.getInstance(con);
        settingValue.loadSettingValue();

        dialog = new ProgressDialog(con);
        dialog.setMessage("Vent venligst..");
        dialog.setCancelable(false);

        txtUsername = findViewById(R.id.txtUsername);
        btnReset = findViewById(R.id.btnReset);
        btnBack = findViewById(R.id.btnBack);

        btnReset.setOnClickListener(this);
        btnBack.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(con, LoginActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnReset:
                if (txtUsername.getText().length() < 1)
                    txtUsername.setError("Indtast brugnavn");
                else
                    ResetPasswordApi();
                break;
            case R.id.btnBack:
                onBackPressed();
                break;
        }
    }

    private void ResetPasswordApi() {


            String tag_json_obj = "json_obj_req";
            dialog.show();
            String url = Api.baseurl + Api.resetpwd + txtUsername.getText().toString();

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,

                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("RESET", "==> " + response);

                            try {
                                JSONObject data = new JSONObject(response);
                                switch (data.getString("text")) {
                                    case "UnknownUser":
                                        Toast.makeText(con, "Ukendt bruger", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "ResetPasswordSend":
                                        Toast.makeText(con, "Vi har sendt en email med et link.", Toast.LENGTH_SHORT).show();
                                        onBackPressed();
                                        break;
                                }
                            } catch (JSONException e) {
                                if (dialog != null)
                                    dialog.dismiss();
                            } catch (Exception e) {
                            }

                            if (dialog != null)
                                dialog.dismiss();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    }) {
            };

            AppController.getInstance().addToRequestQueue(stringRequest, tag_json_obj);

    }
}
