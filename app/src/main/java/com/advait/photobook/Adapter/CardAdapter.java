package com.advait.photobook.Adapter;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.advait.photobook.Activity.NewBottomActivity;
import com.advait.photobook.Activity.PhotoEditingActivity;
import com.advait.photobook.Models.OrderEntry;
import com.advait.photobook.R;
import com.advait.photobook.Utils.SettingValue;
import com.advait.photobook.Utils.SquareImageView;
import com.advait.photobook.common.BaseActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

    private NewBottomActivity ctx;

    ArrayList<OrderEntry> OrderData;
    int id;

    public CardAdapter(NewBottomActivity ctxx, ArrayList<OrderEntry> orderData) {
        this.ctx = ctxx;
        this.OrderData = orderData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_cart, parent, false);
        return new CardAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.lbl1.setText("Antal : " + OrderData.get(position).getQty());

        Glide.with(ctx).load(OrderData.get(position).getLinkImageProduction()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                holder.cart_progress.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                holder.cart_progress.setVisibility(View.GONE);
                return false;
            }
        }).into(holder.image);


        if (OrderData.get(position).getBlackWhite().equals("1"))
            holder.lbl2.setText("Sort/hvid : Ja");
        else
            holder.lbl2.setText("Sort/hvid : Nej");

        if (OrderData.get(position).getWhiteFrame().equals("1"))
            holder.lbl3.setText("Hvid kant : Ja");
        else
            holder.lbl3.setText("Hvid kant : Nej");


        holder.lblRedirect.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                BaseActivity.orderEntry = OrderData.get(position);

//                SettingValue.isNewImage = false;
                SettingValue.isEditFrom = "cart";
                ctx.startActivity(new Intent(ctx, PhotoEditingActivity.class));
            }
        });

//        holder.lbl3.setText(OrderData.get(position).get("white_frame"));
    }

    @Override
    public int getItemCount() {
        return OrderData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        SquareImageView image;
        TextView lbl1, lbl2, lbl3, lblRedirect;
        ProgressBar cart_progress;

        public ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);

            cart_progress = itemView.findViewById(R.id.cart_progress);

            lbl1 = itemView.findViewById(R.id.lbl1);
            lbl2 = itemView.findViewById(R.id.lbl2);
            lbl3 = itemView.findViewById(R.id.lbl3);
            lblRedirect = itemView.findViewById(R.id.lblRedirect);
        }
    }
}