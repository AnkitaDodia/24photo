package com.advait.photobook.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.advait.photobook.R;

import java.util.List;

public class PhotoGalleryAdapter extends RecyclerView.Adapter<PhotoGalleryAdapter.ViewHolder> {

    private Context ctx;
    List<String> imglist;
    int size;


    public PhotoGalleryAdapter(Context ctxx, List<String> list) {
        this.ctx = ctxx;
        this.imglist = list;
    }

    public PhotoGalleryAdapter(Context ctxx, int size) {
        this.ctx = ctxx;
        this.size = size;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_photogallery, parent, false);
        return new PhotoGalleryAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        Glide.with(ctx).load(imglist.get(position)).into(holder.image);

        if (position == 0)
            holder.image.setImageResource(R.drawable.image1);
        else if (position == 2)
            holder.image.setImageResource(R.drawable.image2);
        else if (position == 3)
            holder.image.setImageResource(R.drawable.image3);
        else
            holder.image.setImageResource(R.drawable.image4);
    }

    @Override
    public int getItemCount() {
//        return imglist.size();
        return size;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
        }
    }
}