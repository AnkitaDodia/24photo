package com.advait.photobook.ExpandableGrid;

/**
 * Created by My 7 on 01-Aug-18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Order {

    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("order_date")
    @Expose
    private String orderDate;
    @SerializedName("tracking_link")
    @Expose
    private String trackingLink;
    @SerializedName("NumOrderlines")
    @Expose
    private Integer numOrderlines;
    @SerializedName("orderlines")
    @Expose
    private ArrayList<Orderline> orderlines = null;

    public boolean isExpanded;

    public Order(String name) {
        this.orderDate = name;
        isExpanded = true;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getTrackingLink() {
        return trackingLink;
    }

    public void setTrackingLink(String trackingLink) {
        this.trackingLink = trackingLink;
    }

    public Integer getNumOrderlines() {
        return numOrderlines;
    }

    public void setNumOrderlines(Integer numOrderlines) {
        this.numOrderlines = numOrderlines;
    }

    public ArrayList<Orderline> getOrderlines() {
        return orderlines;
    }

    public void setOrderlines(ArrayList<Orderline> orderlines) {
        this.orderlines = orderlines;
    }

}