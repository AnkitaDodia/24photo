package com.advait.photobook.ExpandableGrid;

/**
 * Created by My 7 on 01-Aug-18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Orderline {

    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("black_white")
    @Expose
    private String blackWhite;
    @SerializedName("white_frame")
    @Expose
    private String whiteFrame;
    @SerializedName("link_image_production")
    @Expose
    private String linkImageProduction;
    @SerializedName("link_image_raw")
    @Expose
    private String linkImageRaw;
    @SerializedName("link_image_thumb")
    @Expose
    private String linkImageThumb;
    @SerializedName("imageprintsize")
    @Expose
    private String imageprintsize;

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getBlackWhite() {
        return blackWhite;
    }

    public void setBlackWhite(String blackWhite) {
        this.blackWhite = blackWhite;
    }

    public String getWhiteFrame() {
        return whiteFrame;
    }

    public void setWhiteFrame(String whiteFrame) {
        this.whiteFrame = whiteFrame;
    }

    public String getLinkImageProduction() {
        return linkImageProduction;
    }

    public void setLinkImageProduction(String linkImageProduction) {
        this.linkImageProduction = linkImageProduction;
    }

    public String getLinkImageRaw() {
        return linkImageRaw;
    }

    public void setLinkImageRaw(String linkImageRaw) {
        this.linkImageRaw = linkImageRaw;
    }

    public String getLinkImageThumb() {
        return linkImageThumb;
    }

    public void setLinkImageThumb(String linkImageThumb) {
        this.linkImageThumb = linkImageThumb;
    }

    public String getImageprintsize() {
        return imageprintsize;
    }

    public void setImageprintsize(String imageprintsize) {
        this.imageprintsize = imageprintsize;
    }
}