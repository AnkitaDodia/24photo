package com.advait.photobook.Fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.advait.photobook.Activity.NewBottomActivity;
import com.advait.photobook.Adapter.CardAdapter;
import com.advait.photobook.Models.CartData;
import com.advait.photobook.Models.CreateOrder;
import com.advait.photobook.Models.OrderEntry;
import com.advait.photobook.R;
import com.advait.photobook.Utils.InternetStatus;
import com.advait.photobook.Utils.SettingValue;
import com.advait.photobook.restinterface.RestInterface;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class FragmentCart extends Fragment implements View.OnClickListener {

    boolean isInternet;

    RecyclerView listPhoto;
    TextView lblPlaceOrder,txtNoOrder,lblTwo,text_price;

    NewBottomActivity con;
    SettingValue settingValue;
    ProgressDialog dialog;

    public static int totalNumberOfImages = 0;

    ArrayList<OrderEntry> orderData = new ArrayList<>();
    Button btn_place_order;
    private static String mPhotoBookName = "";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_cart, container, false);

        con = (NewBottomActivity) getActivity();

        isInternet = new InternetStatus().isInternetOn(con);

        settingValue = SettingValue.getInstance(con);
        settingValue.loadSettingValue();

        listPhoto = rootView.findViewById(R.id.listPhoto);
        lblPlaceOrder = rootView.findViewById(R.id.lblPlaceOrder);
        txtNoOrder = rootView.findViewById(R.id.txtNoOrder);
        lblTwo = rootView.findViewById(R.id.lblTwo);
        text_price = rootView.findViewById(R.id.text_price);
        btn_place_order = rootView.findViewById(R.id.btn_place_order);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(con);
        listPhoto.setLayoutManager(mLayoutManager);
        listPhoto.setItemAnimator(new DefaultItemAnimator());

        lblPlaceOrder.setOnClickListener(this);
        btn_place_order.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onResume() {
        if (isInternet) {
            sendCartListRequest();
        }
        else {
            Toast.makeText(con, "Ingen internetforbindelse", Toast.LENGTH_LONG).show();
        }
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lblPlaceOrder:
                break;

            case R.id.btn_place_order:
                if(orderData.size() > 0)
                {
                    showPhotoBookNameAlert();
                }

                break;
        }
    }

    private int calculatePrice()
    {
        int price = 0,qty = 0;

        for (int i = 0; i < orderData.size(); i++)
        {
            qty = qty + Integer.parseInt(orderData.get(i).getQty());
            Log.e("QTY",""+qty);
        }

        if(qty <= SettingValue.AvailablePhotos)
        {
            // qty is more then free photos

            if(SettingValue.FreeShipments == 0)
            {
                // add 25 when free shipment is not available
                price = 25;
            }
            else
            {
                price = 0;
            }
        }
        else
        {
            int difference  = qty - SettingValue.AvailablePhotos;

            if(SettingValue.FreeShipments == 0)
            {
                // add 25 when free shipment is not available
                price = (difference*2)+25;
            }
            else
            {
                price = (difference*2);
            }
        }
        return price;
    }

    private void sendCartListRequest()
    {
        dialog = new ProgressDialog(con);
        dialog.setMessage("Vent venligst..");
        dialog.setCancelable(false);
        dialog.show();

        try {
            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);



            String jsonStr = "{\"userid\":\""+String.valueOf(settingValue.UserId)+"\"}";

            Log.e("jsonStr cart", "==> " + jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendCartRequest(in, new Callback<CartData>() {
                @Override
                public void success(CartData model, Response response) {
                    if (dialog != null)
                        dialog.dismiss();

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("CART_RESPONSE", "" + new Gson().toJson(model));

                            switch (model.getText()) {
                                case "UnknownUser":
                                    Toast.makeText(con, "Ukendt bruger", Toast.LENGTH_SHORT).show();
                                    break;
                                case "Du har ingen ordrer":
                                    Toast.makeText(con, "Ingen ordrer", Toast.LENGTH_SHORT).show();
                                    txtNoOrder.setVisibility(View.VISIBLE);
                                    break;
                                    case "NoOrderEntry":
                                        txtNoOrder.setText("Du har ingen billeder i kurven");
                                        txtNoOrder.setVisibility(View.VISIBLE);
                                        listPhoto.setVisibility(View.GONE);
                                        break;
                                case "ValidOrdersEntry":
                                    orderData = model.getOrderEntries();

                                    if(orderData.size() > 0) {

                                        txtNoOrder.setVisibility(View.GONE);
                                        listPhoto.setVisibility(View.VISIBLE);

                                        int totalPhoto =  CalclulateTotalPhotos();
                                        lblTwo.setText("Fotos i alt " + totalPhoto);

                                        int price = calculatePrice();
                                        text_price.setText("pris: DKK "+price);

                                        listPhoto.setAdapter(new CardAdapter(con, orderData));
                                    }
                                    else
                                    {
                                        txtNoOrder.setText("Du har ingen billeder i kurven");
                                        txtNoOrder.setVisibility(View.VISIBLE);
                                        listPhoto.setVisibility(View.GONE);
                                    }
                                    break;
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error)
                {
                    if (dialog != null)
                        dialog.dismiss();
                    Log.e("ERROR", "" + error.getMessage());
                    Toast.makeText(con,"Noget gik galt. Prøv igen senere",Toast.LENGTH_LONG).show();
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            if (dialog != null)
                dialog.dismiss();
        }
    }

    public int CalclulateTotalPhotos(){

        int mNoofImages = 0;

//        Log.e("totalNumberOfImages","size : "+orderData.size());

        for(int i = 0; i < orderData.size(); i++){

            mNoofImages = mNoofImages + Integer.parseInt(orderData.get(i).getQty());

            Log.e("totalNumberOfImages","inside loop Qty : "+orderData.get(i).getQty());
        }

        Log.e("totalNumberOfImages","Final Qty : "+mNoofImages);

        return mNoofImages;
    }

    public void CreateOrderAPICall(){

        try {
            dialog = new ProgressDialog(con);
            dialog.setMessage("Vent venligst..");
            dialog.setCancelable(false);
            dialog.show();

            Log.e("UserId",""+String.valueOf(settingValue.UserId));
            Log.e("ordertext",""+mPhotoBookName);
            Log.e("BookName",""+mPhotoBookName);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            String jsonStr = "{\"UserId\":\""+String.valueOf(settingValue.UserId)+"\",\"ordertext\":\""+mPhotoBookName+"\",\"BookName\":\""+mPhotoBookName+"\"}";

            Log.e("jsonStr", "==> " + jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.getCreateOrderRequest(in, new Callback<CreateOrder>() {
                @Override
                public void success(CreateOrder model, Response response) {


                    if (response.getStatus() == 200) {
                        try {
                            Log.e("CreateOrderAPICall", "" + new Gson().toJson(model));

                            switch (model.getText())
                            {
                                case "OrderOk":
//                                    Toast.makeText(con, ""+model.getText(), Toast.LENGTH_SHORT).show();
                                    showOrderDoneDialog();
                                    break;
                                case "OrderError":
                                    Toast.makeText(con, "Der skete en fejl, prøv igen. Gentager fejlen os så kontakt venligst kundeservice", Toast.LENGTH_SHORT).show();
                                     break;
                                case "PaymentFail":
                                    Toast.makeText(con, "Der var problemer med at trække penge på dit kreditkort", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                            if (dialog != null)
                                dialog.dismiss();

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    if (dialog != null)
                        dialog.dismiss();

                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showOrderDoneDialog()
    {
        new AlertDialog.Builder(con)
                .setTitle("24fotos")
                .setMessage("Tak for ordren, vi fremsender billederne til din adresse snarest")
                .setCancelable(false)
                .setPositiveButton("luk", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("MainActivity", "Sending atomic bombs to Jupiter");

                        if (dialog != null) {
                            dialog.dismiss();
                        }

                        startActivity(new Intent(con, NewBottomActivity.class));
                    }
                })
                .show();
    }

    public void showPhotoBookNameAlert(){

        AlertDialog.Builder builder = new AlertDialog.Builder(con);
        builder.setTitle("Vælg en titel til din bog");

        final EditText input = new EditText(con);

        input.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        input.setSingleLine(false);
        input.setHint("F.eks. Familien Frost juli 2018 (max 50 tegn)");
        input.setLines(3);
        input.setMaxLines(5);
        input.setPadding(16,8,16,8);
        input.setGravity(Gravity.LEFT | Gravity.TOP);
        builder.setView(input);


        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter.LengthFilter(50); //Filter to 10 characters
        input .setFilters(filters);

       /* input.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (input.getText().length() >= 50){
                    String str = input.getText().toString().substring(0, 50);
                    input.setText(str);
                    input.setSelection(str.length());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                int wordsLength = countWords(s.toString());// words.length;
//                // count == 0 means a new word is going to start
//                if (count == 0 && wordsLength >= 50) {
//                    setCharLimit(input, input.getText().length());
//                } else {
//                    removeFilter(input);
//                }

//                tvWordCount.setText(String.valueOf(wordsLength) + "/" + 50);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });*/

        builder.setPositiveButton("Send bestilling", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mPhotoBookName = input.getText().toString();
                dialog.cancel();

                if(!mPhotoBookName.toString().trim().isEmpty()){

                    CreateOrderAPICall();

                }else{

                    Toast.makeText(con, "Please type something ",Toast.LENGTH_SHORT).show();
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

        /////

       /* AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View alertDialogView = inflater.inflate(R.layout.dialog_photobookname, null);
        alertDialog.setView(alertDialogView);

        final EditText edt_photobook_name = (EditText) alertDialogView.findViewById(R.id.edt_photobook_name);

        edt_photobook_name.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                int wordsLength = countWords(s.toString());// words.length;
                // count == 0 means a new word is going to start
                if (count == 0 && wordsLength >= 50) {
                    setCharLimit(edt_photobook_name, edt_photobook_name.getText().length());
                } else {
                    removeFilter(edt_photobook_name);
                }

//                tvWordCount.setText(String.valueOf(wordsLength) + "/" + 50);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        alertDialog.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                mPhotoBookName = edt_photobook_name.getText().toString();
                dialog.cancel();
                CreateOrderAPICall();

            }
        });
        alertDialog.show();*/

    }

    private int countWords(String s) {
        String trim = s.trim();
        if (trim.isEmpty())
            return 0;
        return trim.split("\\s+").length; // separate string around spaces
    }

    private InputFilter filter;

    private void setCharLimit(EditText et, int max) {
        filter = new InputFilter.LengthFilter(max);
        et.setFilters(new InputFilter[] { filter });
    }

    private void removeFilter(EditText et) {
        if (filter != null) {
            et.setFilters(new InputFilter[0]);
            filter = null;
        }
    }
}