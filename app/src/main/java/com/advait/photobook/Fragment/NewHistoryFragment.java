package com.advait.photobook.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.advait.photobook.Activity.NewBottomActivity;
import com.advait.photobook.Activity.PhotoEditingActivity;
import com.advait.photobook.ExpandableGrid.ItemClickListener;
import com.advait.photobook.ExpandableGrid.Order;
import com.advait.photobook.ExpandableGrid.OrderHistory;
import com.advait.photobook.ExpandableGrid.Orderline;
import com.advait.photobook.ExpandableGrid.SectionedExpandableLayoutHelper;
import com.advait.photobook.R;
import com.advait.photobook.Utils.InternetStatus;
import com.advait.photobook.Utils.SettingValue;
import com.advait.photobook.common.BaseActivity;
import com.advait.photobook.restinterface.RestInterface;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

/**
 * Created by My 7 on 01-Aug-18.
 */

public class NewHistoryFragment extends Fragment implements ItemClickListener {
    RecyclerView list_order_history;
    TextView txtNoOrder;

    NewBottomActivity con;
    SettingValue settingValue;
    ProgressDialog dialog;

    boolean isInternet;

    ArrayList<Order> orderData = new ArrayList<>();
    ArrayList<Orderline> orderItemData = new ArrayList<>();

    SectionedExpandableLayoutHelper sectionedExpandableLayoutHelper;

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        con = (NewBottomActivity) getActivity();

        isInternet = new InternetStatus().isInternetOn(con);

        settingValue = SettingValue.getInstance(con);
        settingValue.loadSettingValue();

        dialog = new ProgressDialog(con);
        dialog.setMessage("Vent venligst..");
        dialog.setCancelable(false);

        list_order_history = view.findViewById(R.id.list_order_history);
        sectionedExpandableLayoutHelper = new SectionedExpandableLayoutHelper(con,
                list_order_history, this, 3);

        txtNoOrder = view.findViewById(R.id.txtNoOrder);

        if (isInternet) {
            orderlistApi();
        }
        else {
            Toast.makeText(con, "Ingen internetforbindelse", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void itemClicked(Orderline item) {
        BaseActivity.mOrderlineHistory = item;

//        selectedItem.getLinkImageRaw();
        Log.e("getImageprintsize","getImageprintsize : "+BaseActivity.mOrderlineHistory.getImageprintsize());
//        SettingValue.isNewImage = false;
        SettingValue.isEditFrom = "history";
        startActivity(new Intent(getActivity(), PhotoEditingActivity.class));
    }

    @Override
    public void itemClicked(Order section) {

    }

    private void orderlistApi()
    {
        dialog = new ProgressDialog(con);
        dialog.setMessage("Vent venligst..");
        dialog.setCancelable(false);
        dialog.show();

        try {
            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            Log.e("settingValue.UserId", "==> " + settingValue.UserId);

            String jsonStr = "{\"userid\":\""+String.valueOf(settingValue.UserId)+"\"}";

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.orderHistoryRequest(in, new Callback<OrderHistory>() {
                @Override
                public void success(OrderHistory model, retrofit.client.Response response) {
                    if (dialog != null)
                        dialog.dismiss();

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("O_HITORY_RESPONSE---", "" + new Gson().toJson(model));

                            switch (model.getText()) {
                                case "UnknownUser":
                                    Toast.makeText(con, "Ukendt bruger", Toast.LENGTH_SHORT).show();
                                    break;
                                case "Du har ingen ordrer":
                                    Toast.makeText(con, "Ingen ordrer", Toast.LENGTH_SHORT).show();
                                    txtNoOrder.setVisibility(View.VISIBLE);
                                    break;
                                case "NoOrderEntry":
                                    txtNoOrder.setText("Du har ingen billeder i kurven");
                                    txtNoOrder.setVisibility(View.VISIBLE);
                                    list_order_history.setVisibility(View.GONE);
                                    break;
                                case "ValidOrders":
                                    orderData = model.getOrders();

                                    Log.e("orderData.size",""+orderData.size());

                                    for (int i = 0; i < orderData.size(); i++)
                                    {
                                        orderItemData = new ArrayList<>();

                                        String date = orderData.get(i).getOrderDate();

                                        if(orderData.get(i).getOrderlines() != null)
                                        {
                                            orderItemData = orderData.get(i).getOrderlines();
//                                            Log.e("Data","Size : "+orderItemData.get(i).getImageprintsize());
                                        }


                                        sectionedExpandableLayoutHelper.addSection(date, orderItemData);
                                    }

                                    sectionedExpandableLayoutHelper.notifyDataSetChanged();

                                    /*if(orderData.size() > 0) {

                                        txtNoOrder.setVisibility(View.GONE);
                                        listPhoto.setVisibility(View.VISIBLE);

                                        listPhoto.setAdapter(new CardAdapter(con, orderData));
                                        totalNumberOfImages = orderData.size();
                                        lblTwo.setText("Fotos i alt " + totalNumberOfImages);
                                        int price = calculatePrice();
                                        Log.e("PRICE",""+price);
                                        text_price.setText("pris: DKK "+price);
                                    }
                                    else
                                    {
                                        txtNoOrder.setText("Du har ingen billeder i kurven");
                                        txtNoOrder.setVisibility(View.VISIBLE);
                                        listPhoto.setVisibility(View.GONE);
                                    }*/
                                    break;
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error)
                {
                    if (dialog != null)
                        dialog.dismiss();
                    Log.e("ERROR", "" + error.getMessage());
                    Toast.makeText(con,"Noget gik galt. Prøv igen senere",Toast.LENGTH_LONG).show();
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            if (dialog != null)
                dialog.dismiss();
        }
    }

   /*private void orderlistApi() {

        if (UtilsFile.hasConnection(con)) {

            txtNoOrder.setVisibility(View.GONE);

            String tag_json_obj = "json_obj_req";
            dialog.show();
            String url = Api.baseurl + Api.orderlist + settingValue.UserId;
//            String url = Api.baseurl + Api.orderlist + "3";

            Log.e("ORDERLIST_URL", "==> " + url);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,

                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("ORDERLIST", "==> " + response);

                            try {
                                JSONObject data = new JSONObject(response);
                                switch (data.getString("text")) {
                                    case "UnknownUser":
                                        Toast.makeText(con, "Ukendt bruger", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "Du har ingen ordrer":
                                        Toast.makeText(con, "Ingen ordrer", Toast.LENGTH_SHORT).show();
                                        txtNoOrder.setVisibility(View.VISIBLE);
                                        break;
                                    case "ValidOrders":
                                        sectionedExpandableLayoutHelper = new SectionedExpandableLayoutHelper(con,
                                                listDate, NewHistoryFragment.this, 3);

                                        JSONArray jsonarray = new JSONArray();
                                        jsonarray = data.getJSONArray("orders");

                                        Log.e("jsonarray",""+jsonarray);

                                        ArrayList<Item> arrayList = new ArrayList<>();
                                        for(int i = 0; i< jsonarray.length();i++){
                                            arrayList = new ArrayList<>();
                                            JSONArray childArray = jsonarray.getJSONObject(i).getJSONArray("orderlines");

                                            Log.e("childArray",""+childArray);

                                            for(int j = 0;j<childArray.length();j++){
                                                arrayList.add(new Item("08:00 AM", 0,childArray.getJSONObject(j).getString("link_image_thumb"),
                                                        childArray.getJSONObject(j).getString("link_image_raw"),childArray.getJSONObject(j).getInt("qty"),
                                                        childArray.getJSONObject(j).getInt("black_white"),childArray.getJSONObject(j).getInt("white_frame")));
                                            }
                                            sectionedExpandableLayoutHelper.addSection(jsonarray.getJSONObject(i).getString("order_date"), arrayList);
                                        }
                                        sectionedExpandableLayoutHelper.notifyDataSetChanged();

                                        break;
                                }
                            } catch (JSONException e) {
                                if (dialog != null)
                                    dialog.dismiss();
                            } catch (Exception e) {
                            }

                            if (dialog != null)
                                dialog.dismiss();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    }) {
            };

            AppController.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
        }
    }*/
}
