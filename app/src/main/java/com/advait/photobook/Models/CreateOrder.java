package com.advait.photobook.Models;

/**
 * Created by My 7 on 17-Jul-18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateOrder {


    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("orderamt")
    @Expose
    private Integer orderamt;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getOrderamt() {
        return orderamt;
    }

    public void setOrderamt(Integer orderamt) {
        this.orderamt = orderamt;
    }
}