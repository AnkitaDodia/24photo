package com.advait.photobook.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginData {

    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("UserId")
    @Expose
    private int userId;
    @SerializedName("AvailablePhotos")
    @Expose
    private int availablePhotos;
    @SerializedName("FreeShipments")
    @Expose
    private int freeShipments;
    @SerializedName("PriceExtraPhoto")
    @Expose
    private String priceExtraPhoto;
    @SerializedName("PriceExtraShipment")
    @Expose
    private String priceExtraShipment;
    @SerializedName("UrlPhotoRaw")
    @Expose
    private String urlPhotoRaw;
    @SerializedName("UrlPhotoDevelop")
    @Expose
    private String urlPhotoDevelop;
    @SerializedName("UrlPhotoThumb")
    @Expose
    private String urlPhotoThumb;
    @SerializedName("DateNextMembershipRenewal")
    @Expose
    private String dateNextMembershipRenewal;

    @SerializedName("MaxPhotoSize")
    @Expose
    private String maxPhotoSize;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getAvailablePhotos() {
        return availablePhotos;
    }

    public void setAvailablePhotos(int availablePhotos) {
        this.availablePhotos = availablePhotos;
    }

    public int getFreeShipments() {
        return freeShipments;
    }

    public void setFreeShipments(int freeShipments) {
        this.freeShipments = freeShipments;
    }

    public String getPriceExtraPhoto() {
        return priceExtraPhoto;
    }

    public void setPriceExtraPhoto(String priceExtraPhoto) {
        this.priceExtraPhoto = priceExtraPhoto;
    }

    public String getPriceExtraShipment() {
        return priceExtraShipment;
    }

    public void setPriceExtraShipment(String priceExtraShipment) {
        this.priceExtraShipment = priceExtraShipment;
    }

    public String getUrlPhotoRaw() {
        return urlPhotoRaw;
    }

    public void setUrlPhotoRaw(String urlPhotoRaw) {
        this.urlPhotoRaw = urlPhotoRaw;
    }

    public String getUrlPhotoDevelop() {
        return urlPhotoDevelop;
    }

    public void setUrlPhotoDevelop(String urlPhotoDevelop) {
        this.urlPhotoDevelop = urlPhotoDevelop;
    }

    public String getUrlPhotoThumb() {
        return urlPhotoThumb;
    }

    public void setUrlPhotoThumb(String urlPhotoThumb) {
        this.urlPhotoThumb = urlPhotoThumb;
    }


    public String getMaxPhotoSize() {
        return maxPhotoSize;
    }

    public void setMaxPhotoSize(String maxPhotoSize) {
        this.maxPhotoSize = maxPhotoSize;
    }

    public String getDateNextMembershipRenewal() {
        return dateNextMembershipRenewal;
    }

    public void setDateNextMembershipRenewal(String dateNextMembershipRenewal) {
        this.dateNextMembershipRenewal = dateNextMembershipRenewal;
    }
}
