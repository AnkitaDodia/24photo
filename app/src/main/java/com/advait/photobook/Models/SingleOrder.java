package com.advait.photobook.Models;

public class SingleOrder {

    private String qty;
    private String blackWhite;
    private String whiteFrame;
    private String linkImageProduction;
    private String linkImageRaw;
    private String linkImageThumb;
    private String imageprintsize;

    public void SingleOrder(){

    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getBlackWhite() {
        return blackWhite;
    }

    public void setBlackWhite(String blackWhite) {
        this.blackWhite = blackWhite;
    }

    public String getWhiteFrame() {
        return whiteFrame;
    }

    public void setWhiteFrame(String whiteFrame) {
        this.whiteFrame = whiteFrame;
    }

    public String getLinkImageProduction() {
        return linkImageProduction;
    }

    public void setLinkImageProduction(String linkImageProduction) {
        this.linkImageProduction = linkImageProduction;
    }

    public String getLinkImageRaw() {
        return linkImageRaw;
    }

    public void setLinkImageRaw(String linkImageRaw) {
        this.linkImageRaw = linkImageRaw;
    }

    public String getLinkImageThumb() {
        return linkImageThumb;
    }

    public void setLinkImageThumb(String linkImageThumb) {
        this.linkImageThumb = linkImageThumb;
    }

    public String getImageprintsize() {
        return imageprintsize;
    }

    public void setImageprintsize(String imageprintsize) {
        this.imageprintsize = imageprintsize;
    }

}
