package com.advait.photobook.Models;


public class User {

    private String text;
    private String name;
    private String userId;
    private String availablePhotos;
    private String freeShipments;
    private String priceExtraPhoto;
    private String priceExtraShipment;
    private String urlPhotoRaw;
    private String urlPhotoDevelop;
    private String urlPhotoThumb;
    private String dateNextMembershipRenewal;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAvailablePhotos() {
        return availablePhotos;
    }

    public void setAvailablePhotos(String availablePhotos) {
        this.availablePhotos = availablePhotos;
    }

    public String getFreeShipments() {
        return freeShipments;
    }

    public void setFreeShipments(String freeShipments) {
        this.freeShipments = freeShipments;
    }

    public String getPriceExtraPhoto() {
        return priceExtraPhoto;
    }

    public void setPriceExtraPhoto(String priceExtraPhoto) {
        this.priceExtraPhoto = priceExtraPhoto;
    }

    public String getPriceExtraShipment() {
        return priceExtraShipment;
    }

    public void setPriceExtraShipment(String priceExtraShipment) {
        this.priceExtraShipment = priceExtraShipment;
    }

    public String getUrlPhotoRaw() {
        return urlPhotoRaw;
    }

    public void setUrlPhotoRaw(String urlPhotoRaw) {
        this.urlPhotoRaw = urlPhotoRaw;
    }

    public String getUrlPhotoDevelop() {
        return urlPhotoDevelop;
    }

    public void setUrlPhotoDevelop(String urlPhotoDevelop) {
        this.urlPhotoDevelop = urlPhotoDevelop;
    }

    public String getUrlPhotoThumb() {
        return urlPhotoThumb;
    }

    public void setUrlPhotoThumb(String urlPhotoThumb) {
        this.urlPhotoThumb = urlPhotoThumb;
    }

    public String getDateNextMembershipRenewal() {
        return dateNextMembershipRenewal;
    }

    public void setDateNextMembershipRenewal(String dateNextMembershipRenewal) {
        this.dateNextMembershipRenewal = dateNextMembershipRenewal;
    }



}
