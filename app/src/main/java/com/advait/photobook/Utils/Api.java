package com.advait.photobook.Utils;

public class Api {

//    public static String baseurl = "https://36fotos.dk/API/webservices/";
    public static String baseurl = "https://24fotos.dk/API/webservices/";
//    https://24fotos.dk/API/webservices/login.php?email={{EMAIL}}&pw={{password}}&device_id={{device id}}&fcm={{FCM}}
    public static String login = "LoginData.php?email=";
    public static String resetpwd = "resetpassword.php?email=";
    public static String orderlist = "getorderlist.php?userid=";
    public static String delete = "removefrombasket.php?userid=";

    public static String addtobasket = "addtobasket.php";
}