package com.advait.photobook.common;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.FrameLayout;

import com.advait.photobook.ExpandableGrid.Orderline;
import com.advait.photobook.Models.OrderEntry;
import com.advait.photobook.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by My 7 on 17-Jul-18.
 */

public class BaseActivity extends AppCompatActivity
{
    FrameLayout MainFrame;
    public static boolean isFromEditing = false;
    public static OrderEntry orderEntry;
    public static Bitmap OriginalBitmap = null;
    public static String ImageSize = "10 * 10 cm";

    public static Orderline mOrderlineHistory;//Orderline


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MainFrame = findViewById(R.id.MainFrame);
    }

    public void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.MainFrame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public String getFCMID()
    {
        SharedPreferences sp = getSharedPreferences("FCM", MODE_PRIVATE);
        return sp.getString("FCM_ID","Null");
    }


    // Decodes image and scales it to reduce memory consumption
    public Bitmap decodeFile(File f) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // The new size we want to scale to
            final int REQUIRED_SIZE = 300;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            Log.e("scale","scale : "+scale);
            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }
}
