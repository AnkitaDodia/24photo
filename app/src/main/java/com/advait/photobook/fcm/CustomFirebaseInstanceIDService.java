package com.advait.photobook.fcm;

/**
 * Created by Adite-Ankita on 23-Aug-16.
 */

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class CustomFirebaseInstanceIDService extends FirebaseInstanceIdService
{
    private static final String TAG = CustomFirebaseInstanceIDService.class.getSimpleName();


    String refreshedToken;

    @Override
    public void onTokenRefresh() {
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Token_Value: " + refreshedToken);

        saveFCMID(refreshedToken);
    }

    public void saveFCMID(String id) {
        SharedPreferences sp = getSharedPreferences("FCM", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("FCM_ID", id);
        spe.commit();
    }
}