package com.advait.photobook.restinterface;



import com.advait.photobook.ExpandableGrid.OrderHistory;
import com.advait.photobook.Models.CartData;
import com.advait.photobook.Models.CreateOrder;
import com.advait.photobook.Models.LoginData;
import com.advait.photobook.Models.SingleUpload;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.mime.TypedInput;

/**
 * Created by Adite-Ankita on 6/30/2016.
 */
public interface RestInterface
{
    public static String API_BASE_URL = "https://24fotos.dk/API/webservices";

    String CART_LIST = "/getbasketsnew.php";
//    String CREATE_ORDER = "/createorder.php";
    String CREATE_ORDER = "/createorder_new.php";
    String ADD_FCM = "/addfcm.php";
    String ORDER_HISTORY = "/getorderlist_new.php";
    String LOGIN_API = "/loginnew.php";
//    https://24fotos.dk/API/webservices/addtobasketnewsingle.php

    String ADD_TO_BASKET = "/addtobasketnewsingle.php";

//    https://24fotos.dk/API/webservices/createorder_new.php


    //LogIn
    @POST(LOGIN_API)
    public void sendLoginRequest(
            @Body TypedInput typedInput,
            Callback<LoginData> callBack);

    //CART_LIST
    @POST(CART_LIST)
    public void sendCartRequest(
            @Body TypedInput typedInput,
            Callback<CartData> callBack);

    //CREATE_ORDER
    @POST(CREATE_ORDER)
    public void getCreateOrderRequest(@Body TypedInput typedInput, Callback<CreateOrder> callback);

    //{"UserId":"1","ordertext":"my order","BookName":"abhay"}

    //OrderHistory
    @POST(ORDER_HISTORY)
    public void orderHistoryRequest(@Body TypedInput typedInput, Callback<OrderHistory> callBack);

//    jsonObject.put("user_id", settingValue.UserId);//userid
//            jsonObject.put("qty", qty);
//            jsonObject.put("black_white", black_white);
//            jsonObject.put("white_frame", white_frame);
//            jsonObject.put("imageprintsize", ImageSize);
//            jsonObject.put("link_image_raw", OriginalBase64);
//            jsonObject.put("link_image_production", EditedBase64);

 /*   //CREATE_ORDER
    @FormUrlEncoded
    @POST(ADD_TO_BASKET)
    public void getAddtoBasketRequest(@Field("user_id") String user_id, @Field("qty") String qty, @Field("black_white") String black_white, @Field("white_frame") String white_frame,
                                      @Field("imageprintsize") String imageprintsize, @Field("link_image_raw") String link_image_raw, @Field("link_image_production") String link_image_production,
                                      Callback<SingleUpload> callback);*/

    @POST(ADD_TO_BASKET)
    SingleUpload getAddtoBasketRequest(@Body TypedInput typedInput);
}
